# Shop Database Using Sql

## Docker description file.
- Docker Desktop is used in this project to package our application.

## Directory structure:
```
├── src                             the source
│   ├── create-database.sql         the sql sripts for the commands you used for creating the database.
│   ├── create-tables.sql           the sql sripts for the commands used to create the database tables.    
│   ├── insert-tables-records.sql   the sql sripts that contain the commands used for inserting table records.
│   └── query-database.sql          the sql sripts to query data from a database.
├── docker-compose.yaml             docker compose script that runs the whole package in a container.
└── shop-database-description.md    description scripts.
```

## repository guidelines:
### Installation:
- Install Docker desktop from:
```
https://docs.docker.com/
```
- Follow the right guidelines given on the above website for full installation.

- Install PostgreSQL server, learn how to do that on the following websites:
```
https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql/
```

- secondly, you need to have extension called `Docker` installed in the VS code. 
- This repository uses Postgres set-up on the docker desktop, the postgres is the standard DB that most of the tech industries uses. It is one of the images we are going to pull from Docker hub(registry for docker image) using docker-compose/yaml files and run it through our Docker.

### Yaml file:
- The yaml file created is going to do everything for us in vscode terminal when you run the following command:
```
docker-compose up
```
This above command is going to pull up the image, build it and run it on your machine.

- In the yaml file, we have created two servers postgres, version 9.6, which will be running our database created and also adminer which is the web ui for interacting with our database through the ports.

- You can connect to your [adminer] through the ports:
```
http://localhost:8080/
```

### How to run your Database:
- After, you need to connect the database created to your PostgreSQL server through your vscode terminal, 
- Then run the following command and copy the id of the container created that is going to show under the terminal:
```
docker container ls
```
- copy the id of the postgres container and paste it to the following command:
```
docker exec -it <id-of-the-container-or-name-of-the-container> psql -h localhost -U user -d db
``` 

- After run the above command and be connected to your database 'db' local.
Then run your script "create-database.sql":
``` 
\i /docker-entrypoint-initdb.d/create-database.sql
```
This will automatically connect you(as user) to the shop database created.

### The commands in the four sql scripts files created do the followings:
1. Create a database called “shop”, if it exist drop the database and create a new one and set you as the user connected to the 'shop' database.

2. Create the following tables in the `shop` database:

- Customers with nine columns: id, first_name, last_name, gender, address, phone, email, city and country
- Employees with five columns: id, first_name, last_name, email and job_title
- Orders with seven columns: id, product_id, payment_id, fulfilled_by_employee_id, date_required, date_shipped, status
- Payments with four columns: id, customer_id, payment_date, amount
- Products with four columns: id, product_name, description, buy_price

3. Set the `id` columns, of data type `int`, of each tables as the PRIMARY KEY. 

4. Set the following FOREIGN KEYs:
- The column "fulfilled_by_employee_id" of the "orders" table, is the FOREIGN KEY referenced from "employees"'s "id" column. And the "product_id" column of the "orders" table is also the FOREIGN KEY referenced from "products"'s "id" column.
- The column "customer_id" of the payments table is the FOREIGN KEY referenced from "customers"'s "id" column

5. Insert the records of each tables. 

6. Query and update data from the shop database.
