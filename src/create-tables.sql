
/* Create a table called payments inside a database shop*/
DROP TABLE IF EXISTS payments, orders, products, employees, customers CASCADE;
CREATE TABLE customers(
id INT PRIMARY KEY,
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50) NOT NULL,
gender VARCHAR(10) NOT NULL,
address VARCHAR(200) NOT NULL,
phone VARCHAR(20) NOT NULL,
email VARCHAR(100) NOT NULL,
city VARCHAR(20) NOT NULL,
country VARCHAR(50) NOT NULL
);

/* Create a table called employees inside a database shop*/
CREATE TABLE employees(
id INT PRIMARY KEY,
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50) NOT NULL,
email VARCHAR(100) NOT NULL,
job_title VARCHAR(20) NOT NULL
);

/* Create a table called products inside a database shop*/
CREATE TABLE products(
id INT PRIMARY KEY,
product_name VARCHAR(100) NOT NULL,
description VARCHAR(300) NOT NULL,
buy_price DECIMAL(10, 2) NOT NULL
);

/* Create a table called orders inside a database shop*/
CREATE TABLE orders(
id INT PRIMARY KEY,
product_id INT NOT NULL,
payment_id INT NOT NULL,
fulfilled_by_employee_id INT NOT NULL,
date_required DATE NOT NULL,
date_shipped DATE,
status VARCHAR(20) NOT NULL,
FOREIGN KEY (fulfilled_by_employee_id) REFERENCES employees(id),
FOREIGN KEY (product_id) REFERENCES products(id)
);

/* Create a table called payments inside a database shop*/
CREATE TABLE payments(
id INT PRIMARY KEY,
customer_id INT NOT NULL,
payment_date DATE NOT NULL,
amount DECIMAL(10, 2) NOT NULL,
FOREIGN KEY (customer_id) REFERENCES customers(id)
);
