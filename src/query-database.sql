-- query commands


-- 1. SELECT ALL records from table Customers.
SELECT 
    * 
FROM 
    customers;


-- 2. SELECT records only from the name column in the Customers table.
SELECT 
    first_name 
FROM 
    customers;


-- 3. Show the full name of the Customer whose CustomerID is 1.
SELECT 
    first_name, 
    last_name 
FROM customers
WHERE customers.id = 1;


-- 4. UPDATE the record for CustomerID = 1 on the Customer table so that the name is “Lerato Mabitso”.
UPDATE customers
SET first_name = 'Lerato', 
    last_name = 'Mabitso'
WHERE customers.id = 1;


-- 5. DELETE the record from the Customers table for customer 2 (CustomerID = 2).
DELETE FROM customers 
WHERE customers.id = 2;


-- 6. Select all unique statuses from the Orders table and get a count of the number of orders for each unique status.
SELECT 
    status, 
    COUNT(*) AS count 
FROM orders 
GROUP BY status;


-- 7. Return the MAXIMUM payment made on the PAYMENTS table.
SELECT 
    MAX(amount) AS max_amount 
FROM payments;


-- 8. Select all customers from the “Customers” table, sorted by the “Country” column.
SELECT 
    * 
FROM customers 
ORDER BY customers.country; 


-- 9. Select all products with a price BETWEEN R100 and R600.
SELECT * 
FROM 
    payments
WHERE 
    payments.amount BETWEEN 100.00 AND 600.00;


-- 10. Select all fields from “Customers” where the country is “Germany” AND the city is “Berlin”.
SELECT * 
FROM 
    customers 
WHERE 
    customers.country = 'Germany' AND customers.city = 'Berlin'; 


-- 11. Select all fields from “Customers” where the city is “Cape Town” OR “Durban”.
SELECT 
    * 
FROM 
    customers 
WHERE 
    customers.city='Cape Town' OR customers.city='Durban';


-- 12. Select all records from Products where the Price is GREATER than R500.
SELECT 
    * 
FROM 
    products
WHERE 
    products.buy_price > 500;


-- 13. Return the sum of the Amounts on the Payments table.
SELECT 
    CAST(SUM(amount) AS DECIMAL(10, 2))  
    AS 
        sum_amount 
FROM payments;


-- 14. Count the number of shipped orders in the Orders table.
SELECT 
    COUNT(*) 
    AS 
        statuses 
FROM 
    orders
WHERE 
    orders.status = 'Shipped'; 


-- 15. Return the average price of all Products, in Rands and Dollars (assume the exchange rate is R12 to the Dollar).
SELECT 
    CAST(AVG(buy_price) AS DECIMAL(10, 2)) 
    AS 
        average_price_rands, 
    CAST(AVG(buy_price) / 12 AS DECIMAL(10, 2)) 
        AS average_price_dollars
FROM products;


-- 16. Using INNER JOIN create a query that selects all Payments with Customer information.
SELECT 
    payments.id, 
    payments.customer_id, 
    customers.first_name, 
    customers.last_name, 
    customers.email, 
    payments.payment_date, 
    Cast(payments.amount AS DECIMAL(10, 2)) AS amount
FROM 
    payments INNER JOIN customers 
    ON payments.customer_id = customers.id;


-- 17. Select all products that have turnable front wheels.
SELECT 
    *
FROM 
    products 
WHERE 
    products.description ILIKE 'turnable front wheels%';
 