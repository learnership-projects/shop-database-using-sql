/* Insert values inside a customers table of the shop database */ 
INSERT INTO customers (id, first_name, last_name, gender, address, phone, email, city, country)
VALUES
  (1, 'John', 'Hibert', 'Male', '284 chaucer st', '084789657', 'john@gmail.com', 'Johannesburg', 'South Africa'),
  (2, 'Thando', 'Sithole', 'Female', '240 Sect 1', '0794445584', 'thando@gmail.com', 'Cape Town', 'South Africa'),
  (3, 'Leon', 'Glen', 'Male', '81 Everton Rd, Gillits', '0820832830', 'Leon@gmail.com', 'Durban', 'South Africa'),
  (4, 'Charl', 'Muller', 'Male', '290A Dorset Ecke', '+44856872553', 'Charl.muller@yahoo.com', 'Berlin', 'Germany'),
  (5, 'Julia', 'Stein', 'Female', '2 Wernerring', '+448672445058', 'Js234@yahoo.com', 'Frankfurt', 'Germany');

/* Insert values inside a employees table of the shop database */ 
INSERT INTO employees (id, first_name, last_name, email, job_title)
VALUES
  (1, 'Kani', 'Matthew', 'mat@gmail.com', 'Manager'),
  (2, 'Lesly', 'Cronje', 'LesC@gmail.com', 'Clerk'),
  (3, 'Gideon', 'Maduku', 'm@gmail.com', 'Accountant');
  
/* Insert values inside a products table of the shop database */ 
INSERT INTO products (id, product_name, description, buy_price)
VALUES
  (1, 'Harley Davidson Chopper', 'This replica features a working kickstand, front suspension, gear-shift lever', 150.75),
  (2, 'Classic Car', 'Turnable front wheels, steering function', 550.75),
  (3, 'Sportscar', 'Turnable front wheels, steering function', 700.60);
  
/* Insert values inside a orders table of the shop database */ 
INSERT INTO orders (id, product_id, payment_id, fulfilled_by_employee_id, date_required, date_shipped, status)
VALUES
  (1, 1, 1, 2, '2018-09-05', NULL, 'Not shipped'),
  (2, 1, 2, 2, '2018-09-04', '2018-09-03', 'Shipped'),
  (3, 3, 3, 3, '2018-09-06', NULL, 'Not shipped');
  
/* Insert values inside a payments table of the shop database */ 
INSERT INTO payments (id, customer_id, payment_date, amount)
VALUES
  (1, 1, '2018-09-01', 150.75),
  (2, 5, '2018-09-03', 150.75),
  (3, 4, '2018-09-03', 700.60);